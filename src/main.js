import { createApp } from 'vue'
import App from './App.vue'
import logrocket from './api/logrocket'

//estilos globales
import './css/styles.css'
import './css/animations.css'
createApp(App)
.use(logrocket)
.mount('#app')
