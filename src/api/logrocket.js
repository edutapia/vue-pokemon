import LogRocket from 'logrocket';
import LogrocketFuzzySanitizer from 'logrocket-fuzzy-search-sanitizer';
import { privateFieldNames } from './privateData'

const { requestSanitizer, responseSanitizer } = LogrocketFuzzySanitizer.setup(privateFieldNames);
const logRocketConfig = {
    network: {
        requestSanitizer,
        responseSanitizer
    }
}
LogRocket.init(process.env.VUE_APP_LOG_ROCKET_ID, logRocketConfig);

LogRocket.identify('000000001', {
    name: 'Usuario',
    email: 'test1@gmail.com'
});

export default LogRocket;