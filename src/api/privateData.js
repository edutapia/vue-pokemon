export const privateFieldNames = [
    'forms',
    'game_indices',
    'height',
    'held_items',
    'id',
    'is_default',
    'location_area_encounters',
    'moves',
    'name',
    'order',
    'past_type',
    'species',
    'sprites',
    'stats',
    'types',
    'weight'
]